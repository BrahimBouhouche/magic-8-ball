//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by mac on 8/4/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageBall: UIImageView!
    
    var imageIndex : Int = 0
    
    var ballArray = ["ball1","ball2","ball3","ball4","ball5"]
    
    override func viewDidLoad() {
               super.viewDidLoad()
        imageBall.image = UIImage(named : ballArray[imageIndex])
        AskButton()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func hitAskButton(_ sender: Any) {
        AskButton()
    }
    
    func AskButton(){
        imageIndex = Int(arc4random_uniform(5))
        
        imageBall.image = UIImage(named : ballArray[imageIndex])
        
        print(imageIndex)
    }
    
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        AskButton()
    }
    

}

